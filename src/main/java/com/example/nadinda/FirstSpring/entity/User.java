package com.example.nadinda.FirstSpring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity //Anotasi
public class User {

    @Id
    private String username;
    private String password;
    private String name;
    private String email;
//    private Integer umur;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public Integer getUmur() {
//        return umur;
//    }
//
//    public void setUmur(Integer umur) {
//        this.umur = umur;
//    }
}
