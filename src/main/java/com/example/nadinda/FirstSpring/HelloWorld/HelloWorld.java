package com.example.nadinda.FirstSpring.HelloWorld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {

    @RequestMapping ("/dinda")
    public String index(){
        String a = "Hello World";
        return a;
    }
}
