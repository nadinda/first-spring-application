package com.example.nadinda.FirstSpring.repository;

import com.example.nadinda.FirstSpring.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource (path = "/user")
public interface UserRepository extends JpaRepository<User, String> {
}
